# 30/08/2020
# Jason Brown
# Population modelling with sophisticated methods, Environment class



from data.classes.organism import *
from data.classes.event import *

import matplotlib.pyplot as plt
from data.util import *
from numpy import random as rng



class Environment:
    def __init__(self, max_producers, name=None):
        self.max_producers = max_producers
        self.name = name
        self.total_producers = 0
        self.year = 0
        self.organisms = dict()
        self.deltas = dict()
        self.events = list()
        self.continuous_events = list()

    # region str & repr methods
    def __str__(self):
        organism_list = [
            f"""{organism.name} | Population: {organism.population}{f", Food Sources: {list_to_text(list(organism.food_relationships.keys()))}" if type(organism) == Consumer else ""}"""
            for organism in self.organisms.values()
        ]

        text = f"""------------------------------------------------------------
Environment: {self.name}
Size: {self.max_producers}

Organisms:
{list_to_text(organism_list, add_line_break=True) if organism_list != [] else "None"}

Events:
{list_to_text([event.description if event.description is not None else f"{event.organism} event" for event in self.events]) if self.events != [] else "None"}"""
        return text


    def __repr__(self):
        return object.__repr__(self)
    # endregion

    # region UI Functions
    @classmethod
    def new_environment(cls):
        print("Creating a new environment...")
        name = prompt("Name of environment?")
        size = prompt("Size of environment? (Maximum number of producers, recommended 1000-10000)", int)
        return cls(size, name)


    @classmethod
    def load_environment(cls):
        name = prompt("Name of environment?", str)
        env = load_object(name, "environments")

        if not isinstance(env, cls):
            raise Exception(f"Loaded Object {env.__repr__()} was not an Environment object")
        else:
            return env


    def save_environment(self, folder="environments"):
        save_object(self, self.name, folder)


    def reset_sim_graph(self):
        num_years = prompt("Simulate for how many years?", int)
        randomness = prompt("Randomness factor? (Default 0)", float)
        self.reset()
        self.simulate(num_years, verbose=False, timesteps_per_year=100, randomness=randomness)
        self.plot_all_histories(title=self.name)
    # endregion

    # region Plotting
    def plot_history(self, *organisms, title=None):
        if title is None:
            title = self.name

        plt.title(title)
        plt.xlabel("Year")
        plt.ylabel("Population")

        for ref in organisms:
            organism = self.organisms[ref]
            years = [x[0] for x in organism.history]
            populations = [x[1] for x in organism.history]
            plt.plot(years, populations, label=organism.name)
            plt.legend()

        plt.show()


    def plot_all_histories(self, title=None):
        if title is None:
            title = self.name

        organisms = list(self.organisms.keys())
        self.plot_history(*organisms, title=title)
    # endregion

    # region Resetting, Removing & Adding
    def reset(self):
        self.total_producers = 0

        for name, organism in self.organisms.items():
            self.organisms[name].population = organism.initial_population
            self.organisms[name].history = []

            if isinstance(organism, Producer):
                self.total_producers += organism.initial_population

        self.year = 0
        self.deltas = dict()


    def remove_organism(self, organism_name=None):
        if organism_name is None:
            organism_name = prompt("Name of organism?", str, valid_options=list(self.organisms.keys()))

        del self.organisms[organism_name]

        # Removes food relationships
        for organism in self.organisms.values():
            if type(organism) == Consumer:
                if organism_name in organism.food_relationships.keys():
                    organism.remove_food_relationship(organism_name)


    def add_organism(self, organism, start_count=None):
        if start_count is None:  # If we're adding an organism via text ui, we'll need this functionality
            start_count = prompt(f"Starting number of {organism.name}?", int)

        organism.population = start_count
        organism.initial_population = start_count
        self.organisms[organism.name] = organism

        if isinstance(organism, Producer):
            self.total_producers += start_count


    def add_event(self, event):
        if type(event) == Event or type(event) == RecurringEvent:
            self.events.append(event)
        elif type(event) == ContinuousEvent:
            self.continuous_events.append(event)
        else:
            raise Exception(f"Error, event must be an Event object")
    # endregion

    # region Delta & Pop Update functions
    def update_pop(self, name, change):
        self.organisms[name].population += change

        # Update total producers
        if isinstance(self.organisms[name], Producer):
            self.total_producers += change

            # If change brought producer into negative we need to rectify change to total producers
            if self.organisms[name].population < 0:
                self.total_producers -= self.organisms[name].population

        self.organisms[name].population = max(self.organisms[name].population, 0)


    def zero_deltas(self):
        for organism_name in self.organisms.keys():
            self.deltas[organism_name] = 0


    def apply_deltas(self, timesteps_per_year=100, yearly_randomness_mean=1, standard_deviation=0.0):
        for name, delta in self.deltas.items():
            change = delta / timesteps_per_year
            change = change * rng.normal(yearly_randomness_mean, standard_deviation)
            self.update_pop(name, change)


    def calculate_organism_delta(self, name, organism, year, step, timesteps_per_year=100):
        time = year + (step / timesteps_per_year)
        self.organisms[name].history.append([time, organism.population])

        # Get pop change, use of isinstance allows for inheritance for user added extra classes
        if isinstance(organism, Producer):
            self.deltas[name] += organism.get_growth(self.total_producers, self.max_producers)

        elif isinstance(organism, Consumer):
            a, parameter_list = organism.get_growth_parameters()

            top_of_fraction = -a * organism.population
            bottom_of_fraction = organism.population

            for parameters in parameter_list:
                prey_name = parameters[0]
                top_of_fraction += parameters[1] * self.organisms[prey_name].population
                bottom_of_fraction += parameters[2] * self.organisms[prey_name].population
                prey_eaten = self.organisms[prey_name].get_number_eaten_by_predator(organism.population, parameters[3], parameters[4])
                self.deltas[prey_name] -= prey_eaten

            self.deltas[name] += organism.population * (top_of_fraction / bottom_of_fraction)

        else:
            raise Exception(f"Organism: {name} has a type which is not Producer or Consumer")
    # endregion

    # region Simulation functions
    def simulate_next_step(self, step, year, timesteps_per_year, yearly_randomness_mean=1, standard_deviation=0.0, verbose=False):
        # Apply any continous events
        for event in self.continuous_events:
            if event.year <= year < event.year_end:
                event.trigger_event(self, scale_factor=timesteps_per_year, verbose=verbose)

        self.zero_deltas()

        for name, organism in self.organisms.items():
            self.calculate_organism_delta(name, organism, year=year, step=step, timesteps_per_year=timesteps_per_year)

        self.apply_deltas(timesteps_per_year=timesteps_per_year, yearly_randomness_mean=yearly_randomness_mean, standard_deviation=standard_deviation)


    def simulate_next_year(self, year, timesteps_per_year=100, standard_deviation=0.0, verbose=False):
        yearly_randomness_mean = rng.normal(1, (standard_deviation ** 0.5))
        printv(f"Year {year}", verbose)

        # Print current populations
        for name, organism in self.organisms.items():
            printv(f"There are {int(organism.population)} {name}", verbose)

        # Apply any yearly events
        for event in self.events:
            if (type(event) == Event and year == event.year) or (type(event) == RecurringEvent and event.year <= year <= event.year_end and (year - event.year) % event.year_step == 0):
                event.trigger_event(self, verbose=verbose)

        for step in range(timesteps_per_year):
            self.simulate_next_step(step, year, timesteps_per_year, yearly_randomness_mean=yearly_randomness_mean, standard_deviation=standard_deviation, verbose=verbose)


    def simulate(self, num_years, verbose=False, timesteps_per_year=100, randomness=0.0):
        standard_deviation = (timesteps_per_year / 100) * randomness
        for i in range(num_years):
            self.simulate_next_year(self.year, timesteps_per_year=timesteps_per_year, standard_deviation=standard_deviation, verbose=verbose)
            self.year += 1
    # endregion
