# 30/08/2020
# Jason Brown
# Population modelling with sophisticated methods, Organism classes



from data.classes.food_relationship import *

from data.util import *



class Organism:
    def __init__(self, name):
        self.population = 0
        self.initial_population = 0
        self.history = []
        self.name = name

    @classmethod
    def create_organism_and_add_to_env(cls, env):
        org = cls.create_organism(env)
        env.add_organism(org)

    @classmethod
    def create_organism(cls, env=None):
        is_consumer = yes_or_no("Is organism a consumer? (No for producer)")
        if is_consumer:
            return Consumer.create_consumer(env)
        else:
            return Producer.create_producer()

    def get_number_eaten_by_predator(self, predator_pop, d, e):
        return (d * self.population * predator_pop) / ((e * self.population) + predator_pop)



class Producer(Organism):
    def __init__(self, name, max_growth_rate):
        super().__init__(name)
        self.max_growth_rate = max_growth_rate

    def __str__(self):
        text = f"""------------------------------------------------------------
{self.name}

Producer
Population: {self.population}
Max growth rate: {self.max_growth_rate}"""
        return text

    @classmethod
    def create_producer(cls):
        name = prompt("Name of producer:", str)
        max_growth_rate = prompt("Maximum growth rate (Fraction by which population increases each year, e.g. 0.1 is 10% growth per year, recommended is ~0.5):", float)
        org = cls(name=name, max_growth_rate=max_growth_rate)
        return org

    def get_growth(self, current_producer_population, max_population):
        # Calculated via logistic equation, deaths due to consumers is handled consumer side
        return self.max_growth_rate * self.population * (1 - (current_producer_population / max_population))



class Consumer(Organism):
    def __init__(self, name, max_birth_rate, starving_death_rate):
        super().__init__(name)
        self.max_birth_rate = max_birth_rate
        self.starving_death_rate = starving_death_rate
        self.food_relationships = dict()


    def __str__(self):
        text = f"""============================================================
{self.name}

Consumer
Population: {self.population}
Max birth rate: {self.max_birth_rate}
Starving death rate: {self.starving_death_rate}

------------------------------------------------------------
{self.food_sources_str()}"""
        return text


    def food_sources_str(self):
        text = f"""Food Sources:
{list_to_text([f"{linebreak}{food_source_obj}" for food_source_obj in self.food_relationships.values()], add_line_break=True)}"""
        return text


    @classmethod
    def create_consumer(cls, env=None):
        name = prompt("Name of consumer:", str)
        max_birth_rate = prompt("Maximum birth rate (Fraction by which population increases each year, e.g. 0.1 is 10% growth per year, recommended is 0.2):", float)
        starving_death_rate = prompt("Starving death rate (Fraction of population that dies each year if starving e.g. 12 is all dead within ~1 month, recommended is ~12):", float)
        org = cls(name=name, max_birth_rate=max_birth_rate, starving_death_rate=starving_death_rate)

        while True:
            print(f"""\nCurrent food sources: {list_to_text([food.name for food in org.food_relationships.values()]) if org.food_relationships != {} else "None"}""")
            add_another_food_source = yes_or_no(f"""Add a food source? {"(At least 1 required for simulation):" if org.food_relationships == {} else ""}""")

            if add_another_food_source:
                org.add_food_relationship_via_ui(env)
            else:
                break

        return org


    def add_food_relationship_obj(self, food):
        self.food_relationships[food.name] = food


    def add_food_relationship(self, name_of_food, zero_growth_ratio, max_number_eaten_by_individual, max_chance_individual_is_eaten):
        food_relationship_obj = FoodRelationship(name_of_food, zero_growth_ratio, max_number_eaten_by_individual, max_chance_individual_is_eaten)
        self.add_food_relationship_obj(food_relationship_obj)


    def add_food_relationship_via_ui(self, env=None):
        name_of_food = prompt("Name of food source?", str, valid_options=(list(env.organisms.keys()) if env is not None else None))
        zero_growth_ratio = prompt("Zero growth ratio? (Ratio of eater to food at which eater population doesn't change, recommended is ~0.5):", float)
        max_number_eaten_by_individual = prompt("Max number of food population that each eater eats if food is abundant, recommended is ~1:", float)
        max_chance_individual_is_eaten = prompt("Max chance individual food is eaten in a year if eaters are abundant, recommended is ~1:", float)

        self.add_food_relationship(name_of_food=name_of_food, zero_growth_ratio=zero_growth_ratio, max_number_eaten_by_individual=max_number_eaten_by_individual, max_chance_individual_is_eaten=max_chance_individual_is_eaten)


    def remove_food_relationship(self, food_source):
        del self.food_relationships[food_source]


    def get_growth_parameters(self):
        # See https://en.wikipedia.org/wiki/Talk:Lotka%E2%80%93Volterra_equations
        # Predator constants: a is death rate constant, b and c are from the food relationship
        # Prey constants: d and e used to calc impact on prey
        a = self.starving_death_rate

        # [[Prey name, b, c, d, e], ...]
        parameter_list = []

        for food, relationship in self.food_relationships.items():
            b = a * relationship.zero_growth_ratio
            c = b / self.max_birth_rate
            d = relationship.max_chance_individual_is_eaten
            e = d / relationship.max_number_eaten_by_individual
            parameter_list.append([food, b, c, d, e])

        return a, parameter_list
