# 30/08/2020
# Jason Brown
# Population modelling with sophisticated methods, Event class



class Event:
    def __init__(self, year, organism, value, event_type, description=None):
        self.year = year
        self.organism = organism
        self.value = value  # If scale this is the scaling factor ie. 0.9 for 10% decrease
        self.event_type = event_type  # Options are change, set and scale
        self.description = description

    def __str__(self):
        return self.description if self.description is not None else f"{self.organism} event"

    def trigger_event(self, environment, scale_factor=1, verbose=False):
        if verbose:
            print(f"Event {self} triggered")

        if self.event_type == "change":
            calculated_change = self.value
        elif self.event_type == "set":
            calculated_change = self.value - environment.organisms[self.organism].population
        elif self.event_type == "scale":
            calculated_change = environment.organisms[self.organism].population * (self.value - 1)
        else:
            raise Exception("Error, invalid event type, must be \"change\", \"set\" or \"scale\"")

        # Adjust if it's continuous
        calculated_change /= scale_factor

        # Update new population safely
        environment.update_pop(name=self.organism, change=calculated_change)


class RecurringEvent(Event):
    def __init__(self, year_start, year_end, year_step, organism, value, event_type, description=None):
        super().__init__(year_start, organism, value, event_type, description)
        self.year_end = year_end  # Inclusive
        self.year_step = year_step


class ContinuousEvent(Event):
    def __init__(self, year_start, year_end, organism, value, event_type, description=None):
        super().__init__(year_start, organism, value, event_type, description)
        self.year_end = year_end  # Exclusive
