# 30/08/2020
# Jason Brown
# Population modelling with sophisticated methods, Food relationship class


class FoodRelationship:
    def __init__(self, name_of_food, zero_growth_ratio, max_number_eaten_by_individual, max_chance_individual_is_eaten):
        self.name = name_of_food
        self.zero_growth_ratio = zero_growth_ratio
        self.max_number_eaten_by_individual = max_number_eaten_by_individual
        self.max_chance_individual_is_eaten = max_chance_individual_is_eaten

    def __str__(self):
        text = f"""{self.name}
Zero growth ratio: {self.zero_growth_ratio}
Max number eaten by individual eater: {self.max_number_eaten_by_individual}
Max chance individual food is eaten: {self.max_chance_individual_is_eaten}"""
        return text
