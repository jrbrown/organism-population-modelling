# 14/05/2020
# Jason Brown
# Util module


import numpy as np
import pickle
import os
from multiprocessing import Process
import string
from datetime import datetime


# region Useful miscellaneous functions
def convert_base(number, parent_base=10, target_base=10, parent_zero=None, target_zero=None):
    """
    Converts a number into a different base via a provided definition of that base
    WARNING Doesn't seem to quite work past the first ~14+ digits, no problem if number is shorter than that

    :param number: int or string; Number to convert
    :param parent_base: string, list or int; Contains all characters in base number is already in
    :param target_base: string, list or int; Contains all characters in base that we are converting to
    :param parent_zero: bool; Whether parent base contains a 0, if None is set to true if first digit of base is 0
    :param target_zero: bool; Whether target base contains a 0, if None is set to true if first digit of base is 0
    :return: string or int; The number represented in the provided base, int if target is base 10
    """

    if isinstance(parent_base, int):
        if parent_base > 10 or parent_base < 1:
            raise Exception("Error in conv_base, parent base not 1-10 without explicit definition"
                            f"\nParent base = {parent_base}")

    if isinstance(target_base, int):
        if target_base > 10 or target_base < 1:
            raise Exception("Error in conv_base, target base not 1-10 without explicit definition"
                            f"\nTarget base = {target_base}")

    # If a base is passed as either a string or integer this converts it to a list
    def convert_int_and_str_bases(base, zero):

        if isinstance(base, str):
            base = list(base)
        elif isinstance(base, int):
            base = [str(x) for x in range(0, base)]
        else:
            raise Exception(f"Error, both bases must either be an integer, list or string, this base was invalid: {base}")

        zero = (base[0] == "0") if zero is None else zero

        return base, zero


    if parent_base != 10:  # Convert to base 10 if not in base 10 so our algorithm for converting to target base works
        parent_base, parent_zero = convert_int_and_str_bases(parent_base, parent_zero)

        number = list(str(number))  # Makes sure number is correct format

        number_base10 = 0
        for power, digit in enumerate(number[::-1]):

            try:
                digit_value = parent_base.index(str(digit))
            except ValueError:
                raise Exception(f"Digit {digit} was invalid for parent base. Parent base has digits: {''.join(parent_base)}")

            if not parent_zero:
                digit_value += 1
            number_base10 += digit_value * (len(parent_base) ** int(power))

        number = number_base10

    if target_base != 10:  # Convert base 10 number into target base
        target_base, target_zero = convert_int_and_str_bases(target_base, target_zero)

        if number < 0:
            raise Exception(f"Error, only supports positive numbers, number = {number}")

        # Offsetting allows non-zeroed bases i.e. a, b, ... z, aa, ab ... compared to a, b, ... z, ba, bb ...
        offset = 1
        if target_zero:
            offset = 0

        new_string_list = []
        base_size = len(target_base)

        while number > 0:
            index = (number - offset) % base_size
            new_string_list.insert(0, target_base[index])
            number = (number - index - offset) // base_size

        number = ''.join(new_string_list)

    return number


def dict_swap(dictionary):
    """
    Swaps the keys and values of a given dictionary

    :param dictionary: Dictionary to use
    :return: Dictionary that has keys and values switched
    """
    return dict((v, k) for k, v in dictionary.items())


def execute_in_parallel(function, *args, **kwargs):
    """
    Execute a function in parallel to code where it was executed from. Doesn't yet join or end process so be careful
    Mainly used for putting up graphs and images whilst code is still executing
    Function MUST be accessible from main scope i.e. is not nested in another function. It can be a method from a class

    :param function: Function to execute
    :param args: Arguments for that function
    :param kwargs: Keyword arguments for that function
    """
    process = Process(target=function, args=args, kwargs=kwargs)
    process.start()


def get_time_stamp():
    date = datetime.now().date().__str__()
    time = datetime.now().time().__str__()
    time = '-'.join(time.split(':')[:2])
    return f"{date}_{time}"


class LogUsage:
    # This class is to be used as a function decorator (@LogUsage) that when activated (LogUsage.active = True) will log
    # whenever a tagged function is called, the amount of times it's been called and when the function is finished
    active = False

    def __init__(self, func):
        self.func = func
        self.counter = 0

    def __call__(self, *args, **kwargs):
        self.counter += 1

        # Check whether to print debug statement
        if self.active:
            print(f"{self.func.__name__} has been called, total: {self.counter}")

        # Execute function
        self.func(*args, **kwargs)

        # Check whether to print debug statement
        if self.active:
            print(f"{self.func.__name__} has finished")
# endregion


# region List choice functions
def norm_list_probabilities(_list, print_new_list=False):
    """
    Takes _list destined for choose_from_list and normalises their probs to equal 1

    :param _list: list; Target list
    :param print_new_list: bool; Whether to print new list or not
    :return: dict; List with probs summing to 1
    """
    values = [x[1] for x in _list]

    if np.sum(values) == 0:
        raise ValueError(f"Values in the list sent to norm_list_probabilities summed to 0: {_list}")

    magnitude = np.linalg.norm(np.array(values), ord=1)
    new_list = [[item, probability/magnitude] for item, probability in _list]

    if print_new_list:
        print(f"New list: {new_list}")

    return new_list


def choose_from_list(_list, npr=None, verbose=False):
    """
    Picks an item from a list where the list items are 2 element lists of item and probability

    :param _list: list; Target list
    :param npr: numpy.random obj; Random number generator to use, allows seeding from where it's called
    :param verbose: bool; Whether to print out warning info
    :return: dictionary key; The chosen key
    """

    if npr is None:
        npr = np.random

    # Allows for list of probabilities to form an integer selection
    if not isinstance(_list[0], (list, tuple)):
        _list = list(zip(range(len(_list)), _list))

    # This loop is to catch and correct sum of probs != 1
    while True:
        if round(float(np.sum([x[1] for x in _list])), 5) != 1.00000:  # List comprehension returns list of probs
            if verbose:
                print("Warning in choose_from_list, values did not add to 1, normalising")
                print(f"Old list: {_list}")
            _list = norm_list_probabilities(_list, print_new_list=verbose)
            verbose = True  # We should never come back here so we'll go verbose to make it obvious we looped infinitely
        else:
            break  # Free to carry on to rest of function

    roll = npr.random()

    for item, probability in _list:
        roll -= probability
        if roll < 0:  # Item was chosen
            return item
    else:
        raise Exception(f"An item was not chosen in the list {_list} where the roll was {roll} from npr {npr}")
# endregion


# region Text UI
linebreak = "\n"  # To be used in f"""These sorts of strings{linebreak}May not make sense but it's useful in ternary operators"""


def menu(question, menu_list, question_header=0, repeat=False, disable_auto_exit=False, manual_repeating=False):
    """
    Menu list of format [[option_text_1, function_to_execute_1, function_1_args_list, function_1_kwargs_dict], ...]
    Passing _question as a lambda function will call it before each menu print - allows for dynamic question generation
    This also works for option texts
    Function args are optional
    If repeating it will stop if a function returns "break_menu" as first return value
    Exit is automatically supplied as first option if repeat is True, unless disable_auto_exit is True
    Returns all the return values of executed function, except the first return value if it's "break_menu" and was used to
    stop repeating the menu
    LEGACY FEATURE:
    Manual repeating should be used if texts of question or options need to dynamically change without exiting menu
        while menu(...) != "break_menu": pass
    NEW METHOD: Make question or option texts callable functions that return the desired text e.g.
        _question = lambda: f"This is a question with some variable information {variable_info}"
    """

    if (repeat or manual_repeating) and not disable_auto_exit:
        menu_list.insert(0, ["Exit", lambda: "break_menu"])

    while True:

        # Dynamic question generation support: Checks if question is a function to be called to dynamically generate question text
        _question = question() if callable(question) else question

        if question_header == 2:
            print(f"""============================================================
{_question}
------------------------------------------------------------
""")
        elif question_header == 1:
            print(f"""------------------------------------------------------------
{_question}
""")
        else:
            print(_question+"\n")

        for i, option in enumerate(menu_list):
            option_text = option[0]
            _option_text = option_text() if callable(option_text) else option_text
            print(f"{i}. {_option_text}")

        while True:
            user_choice = input()
            print()

            try:
                user_choice = int(user_choice)
            except ValueError:
                print("Input not an integer")
            else:
                if user_choice >= len(menu_list) or user_choice < 0:
                    print("Choice out of range")
                else:
                    break

        choice = menu_list[user_choice]

        if not choice[2:3] or isinstance(choice[2], dict):  # Checks for args and inserts empty list if none
            choice.insert(2, [])

        if not choice[3:4]:  # Checks for kwargs and inserts empty list if none
            choice.insert(3, dict())

        if not isinstance(choice[2], list):  # If single argument was passed we'll make a single item list of it
            choice[2] = [choice[2]]

        return_value = choice[1](*choice[2], **choice[3])  # Executes function giving args and kwargs

        break_menu = False

        # Determine whether to filter break_menu string from return text
        if not manual_repeating:
            if isinstance(return_value, str):
                if return_value == "break_menu":
                    return_value = None
                    break_menu = True

            elif isinstance(return_value, tuple):
                if return_value[0] == "break_menu":
                    return_value = return_value[1] if len(return_value) == 2 else return_value[1:]
                    break_menu = True

        if not repeat or break_menu is True:
            break

    return return_value


def prompt(question, ans_type=None, valid_options=None, valid_options_case_sensitive=True):
    """
    Asks the user a question via input() and will keep asking until an appropriate answer is given

    :param question: Question text to use
    :param ans_type: Type expected for the answer, if input does not convert to this type the question is asked again
    :param valid_options: Optionally provide a list of valid options for the user to enter. If using this consider if a
                            menu is more appropriate. If user input is not in this list question is asked again
    :param valid_options_case_sensitive: Whether or not to care about case when checking if the answer is a valid option
    :return: What the user inputted after they provide an answer conforming to the given constraints
    """
    while True:
        answer = input(f"{question} ")

        if ans_type is not None:
            try:
                answer = ans_type(answer)
            except ValueError:
                print(f"Error, incorrect type entered, expected {ans_type}")
                continue

        if isinstance(valid_options, list):
            if (answer not in valid_options) and (valid_options_case_sensitive or answer.lower() not in [option.lower() for option in valid_options]):
                print(f"Error, answer is not a valid option, valid options are: {list_to_text(valid_options)}")
                continue

        break

    return answer


def yes_or_no(question):
    """
    Asks the user a yes or no question. Valid responses: Yes, No, y, n and all case variants of these.
    If an invalid response is given it loops until a valid one is provided

    :param question: The question to ask
    :return: A bool which is true if user replied yes and false if they said no
    """
    while True:
        answer = input(f"{question} y/n: ").lower()

        if answer == "y" or answer == "yes":
            return True
        elif answer == "n" or answer == "no":
            return False
        else:
            print("Invalid input, please enter yes, no, y or n")


def enter_to_continue():
    """
    Little function that allows for staggering what's being printed
    """
    input("\nPress enter to continue... ")


def print_info(info):
    """
    Prints and then waits for user to press enter to continue

    :param info: Thing to print
    """
    print(info)
    enter_to_continue()


def printv(fstring, verbose_flag):
    """
    Prints a string only if the flag is true. Can be used as a debug printer

    :param fstring: String to print
    :param verbose_flag: Flag to decide if to print or not
    """
    if verbose_flag:
        print(fstring)
# endregion


# region Object functions
def save_object(_object, filename, folder=None):
    """
    Saves a python object as a .pkl file in the given folder with the given filename.
    Folder path starts at wherever the if __name__ == '__main__' was executed from.

    :param _object: Object to save
    :param filename: Name of .pkl file
    :param folder: Folder to place it in
    """
    filename = f"{filename}.pkl"
    path = os.path.join(folder, filename)

    with open(path, 'wb') as _output:
        pickle.dump(_object, _output)


def load_object(filename, folder):
    """
    Load an object in a .pkl file that was created via save_object
    Folder path starts at wherever the if __name__ == '__main__' was executed from.

    :param filename: Name of .pkl file
    :param folder: Folder it's located
    :return: The object to load
    """
    filename = f"{filename}.pkl"
    path = os.path.join(folder, filename)

    with open(path, 'rb') as _input:
        _object = pickle.load(_input)

    return _object


def rename_object(_object):
    """
    Change the name property of an object via text ui. Can be called even if it doesn't already exist to give it a name

    :param _object: Object to change the name property of
    """
    old_name = getattr(_object, "name", None)
    new_name = prompt(f"Current name is {old_name}\nNew name?\n")
    setattr(_object, "name", new_name)


def redefine_object_property(_object, _property):
    """
    Change a generic property of an object via text ui. New property value must cast to same type as previous value,
    unless previous value was None. If property doesn't exist it's created.

    :param _object: Object to change
    :param _property: Property of that object to change
    """

    # Get old type and property
    old_property = getattr(_object, _property, None)
    old_type = type(old_property)

    # Get new value and set type
    new_property = prompt(f"Current value of {_property} is {old_property}\nNew value?\n", old_type)
    new_property = old_type(new_property) if old_property is not None else new_property

    # Assign value to property
    setattr(_object, _property, new_property)
# endregion


# region String conversions and operations
def dict_to_string(_dict, cap_and_space_key=False, cap_and_space_value=False, all_on_one_line=False):
    """
    Converts a dictionary into a string of all its properties. Each property is separated by "\n" or ", " depending on
    all_on_one_line parameter

    Example:
    >> a = {"property_1": value_1, "property_2": value_2}
    >> print(dict_to_string(a, cap_and_space=True))
    Property 1: value_1
    Property 2: value_2

    :param _dict: Dictionary to use
    :param cap_and_space_key: Whether to capitalise and space the dictionary keys (see capitalise_and_space)
    :param cap_and_space_value: Whether to capitalise and space the dictionary values
    :param all_on_one_line: Whether to have all properties on one line or on newlines
    :return: The created string
    """
    _string = ""

    for k, v in _dict.items():
        k = capitalise_and_space(k) if cap_and_space_key else k
        v = capitalise_and_space(v) if cap_and_space_value else v
        end = ", " if all_on_one_line else "\n"
        _string += f"{k}: {v}{end}"

    _string = _string[:-1] if _string[-1] == "\n" else _string[:-2]  # Removes final "\n" or ", "

    return _string


def capitalise_and_space(_string):
    """
    Takes "a_string_like_this" and makes it "A String Like This"

    :param _string: String to capitalise and space
    :return: Capitalised and spaced string
    """
    # Converts stuff_like_this to Stuff Like This
    return string.capwords(' '.join(_string.split('_')))


def comma_number(number):
    """
    Takes numbers like this 1000000 and makes a string like this 1,000,000

    :param number: Number to apply commas to
    :return: String that is number with commas
    """
    dig_list = list(str(number))
    comma_num_list = []

    for i, dig in enumerate(dig_list[::-1]):  # Go from LSD to MSD as we want to count 3 digits from LSD
        if i % 3 == 0:  # This ends up putting a comma in on first iter, we remove this later
            comma_num_list.append(',')
        comma_num_list.append(dig)

    comma_num = ''.join(comma_num_list[::-1][:-1])  # We need to reverse back and remove comma that was put at end
    return comma_num


def list_to_text(_list, cap_and_space_value=False, add_line_break=False):
    """
    Converts a list to text. E.g. ["John", "James", "Jango"] -> "John, James, Jango"

    :param _list: List to convert
    :param cap_and_space_value: Whether to capitalise and space the list values (see capitalise_and_space)
    :param add_line_break: Whether to add a linebreak between items
    :return: The created string
    """
    try:
        _string = _list[0]
    except IndexError:
        _string = ""
    else:
        for thing in _list[1:]:
            thing = capitalise_and_space(thing) if cap_and_space_value else thing
            _string += ("\n" if add_line_break else ", ") + thing
    return _string
# endregion
