# 30/08/2020
# Jason Brown
# Population modelling with sophisticated methods, Examples & API file


from data.classes.environment import *
from data.classes.organism import *
from data.classes.food_relationship import *
from data.classes.event import *


# region Interesting solutions
# region Predator-Prey

"""
Notes on 2 organism interaction:
2 Steady state solutions, everything steady and alive and everything dying out

Between these 2 solutions lie fluctuating solutions, the closer to everything dying out the more extreme and less
atenuated these fluctuations become, eventually hitting a lokta-volterra style harmonic solution

The following parameter changes will push the solution to everything steady and alive, the opposite will push towards
harmonic solutions / death:
+max_growth_rate
-max_birth_rate
-zero_growth_ratio
-max_number_eaten_by_individual
-max_chance_individual_is_eaten

Nb, regardless of other constraints if max_chance_individual_is_eaten is less than max_growth_rate, producer can never
die out. If they are equal there are solutions where the prey pop features exponential decay
"""  # Notes


def pp_steady():
    carrots = Producer("Carrot", max_growth_rate=0.5)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.02, max_number_eaten_by_individual=30, max_chance_individual_is_eaten=0.5)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def pp_oscillate_then_steady():
    carrots = Producer("Carrot", max_growth_rate=0.5)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.02, max_number_eaten_by_individual=30, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def pp_slowly_attenuating_oscillations():
    carrots = Producer("Carrot", max_growth_rate=0.53)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.02, max_number_eaten_by_individual=40, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(1000, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def pp_oscillate():
    carrots = Producer("Carrot", max_growth_rate=0.5)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.02, max_number_eaten_by_individual=40, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def pp_death():
    carrots = Producer("Carrot", max_growth_rate=0.5)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.02, max_number_eaten_by_individual=50, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def pp_exponential_decay():
    carrots = Producer("Carrot", max_growth_rate=0.5)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.2, max_number_eaten_by_individual=1000, max_chance_individual_is_eaten=0.5)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 10)

    env.simulate(5000, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()

# endregion


# region Predator-Prey-Plant

def ppp_steady():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    foxes = Consumer("Fox", max_birth_rate=0.1, starving_death_rate=1)
    foxes.add_food_relationship("Rabbit", zero_growth_ratio=0.1, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(foxes, 20)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def ppp_oscillate_then_steady():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    foxes = Consumer("Fox", max_birth_rate=0.1, starving_death_rate=1)
    foxes.add_food_relationship("Rabbit", zero_growth_ratio=0.2, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(foxes, 20)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def ppp_prey_dies_out():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    foxes = Consumer("Fox", max_birth_rate=0.1, starving_death_rate=1)
    foxes.add_food_relationship("Rabbit", zero_growth_ratio=0.3, max_number_eaten_by_individual=1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(foxes, 20)

    env.simulate(500, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()

# endregion


# region Cool
def cool1():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    wolf = Consumer("Fox", max_birth_rate=0.1, starving_death_rate=1)
    wolf.add_food_relationship("Rabbit", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)
    wolf.add_food_relationship("Sheep", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 180)
    env.add_organism(wolf, 10)

    env.simulate(200, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def steady_5_body():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    wolf = Consumer("Wolf", max_birth_rate=0.1, starving_death_rate=1)
    wolf.add_food_relationship("Rabbit", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)
    wolf.add_food_relationship("Sheep", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 180)
    env.add_organism(wolf, 10)

    env.simulate(500, verbose=False, timesteps_per_year=100)
    env.plot_all_histories()


def steady_balanced_4_body():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 180)

    env.simulate(200, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def imbalanced_4_body():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.5, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 90)

    env.simulate(200, verbose=True, timesteps_per_year=100)
    env.plot_all_histories()


def wolf_acting_as_stabiliser():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    wolf = Consumer("Wolf", max_birth_rate=0.1, starving_death_rate=1)
    wolf.add_food_relationship("Rabbit", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)
    wolf.add_food_relationship("Sheep", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)

    env = Environment(10000, "5 Body Steady")
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 90)
    env.add_organism(wolf, 10)

    wolf_cull = Event(200, "Wolf", -200, "change", description="Wolf cull")
    env.add_event(wolf_cull)

    env.simulate(500, verbose=False, timesteps_per_year=100)
    env.plot_all_histories()


def lower_level_style_execution():
    carrots = Producer("Carrot", max_growth_rate=0.4)

    rabbits = Consumer("Rabbit", max_birth_rate=0.2, starving_death_rate=1)
    rabbits.add_food_relationship("Carrot", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    grass = Producer("Grass", max_growth_rate=0.4)

    sheep = Consumer("Sheep", max_birth_rate=0.2, starving_death_rate=1)
    sheep.add_food_relationship("Grass", zero_growth_ratio=0.5, max_number_eaten_by_individual=0.25, max_chance_individual_is_eaten=1)

    wolf = Consumer("Wolf", max_birth_rate=0.1, starving_death_rate=1)
    wolf.add_food_relationship("Rabbit", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)
    wolf.add_food_relationship("Sheep", zero_growth_ratio=0.25, max_number_eaten_by_individual=0.1, max_chance_individual_is_eaten=1)

    env = Environment(10000)
    env.add_organism(carrots, 2000)
    env.add_organism(rabbits, 200)
    env.add_organism(grass, 1800)
    env.add_organism(sheep, 90)
    env.add_organism(wolf, 10)

    # Simulating with 'wolf culls'
    years = 500
    cull_threshold = 400
    cull_amount = 250

    # Main simulating loop
    for year in range(years):
        # Check if wolves need culling
        if env.organisms["Wolf"].population >= cull_threshold:
            env.organisms["Wolf"].population -= cull_amount

        # Simulate the rest of the year
        env.simulate_next_year(year)

    env.plot_all_histories()

# endregion

# endregion


if __name__ == '__main__':
    pass
