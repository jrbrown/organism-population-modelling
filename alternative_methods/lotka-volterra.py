# 23/07/2020
# Jason Brown
# Population modelling with lotka-volterra


import matplotlib.pyplot as plt
from data.util import printv



class Producer:

    is_producer = True

    def __init__(self, name, reproduction_factor=None):
        self.population = 0
        self.history = []
        self.name = name
        if reproduction_factor is not None:
            self.reproduction_factor = reproduction_factor


class Consumer(Producer):

    is_producer = False

    def __init__(self, name, food, death_rate):
        super().__init__(name)
        self.food = food
        self.death_rate = death_rate



class Food:
    def __init__(self, name, hunting_factor, biomass_ratio):
        self.name = name
        self.hunting_factor = hunting_factor
        self.biomass_ratio = biomass_ratio  # Predator : Prey



class Environment:
    def __init__(self, max_producers):
        self.max_producers = max_producers
        self.total_producers = 0
        self.organisms = dict()
        self.deltas = dict()

    def plot_history(self, *organisms):
        plt.title("History")
        plt.xlabel("Day")
        plt.ylabel("Population")
        # plt.grid()

        for ref in organisms:
            organism = self.organisms[ref]
            days = [x[0] for x in organism.history]
            populations = [x[1] for x in organism.history]
            plt.plot(days, populations, label=organism.name)
            plt.legend()

        plt.show()

    def plot_all_histories(self):
        organisms = list(self.organisms.keys())
        self.plot_history(*organisms)

    def add_organism(self, organism, start_count):
        organism.population = start_count
        self.organisms[organism.name] = organism

        if organism.is_producer:
            self.total_producers += start_count

    def simulate(self, num_days, verbose=False, timesteps_per_day=1000):
        for day in range(num_days):
            printv(f"Day {day}", verbose)

            # Print current populations
            for name, organism in self.organisms.items():
                printv(f"There are {int(organism.population)} {name}", verbose)

            # Apply days timesteps
            for step in range(timesteps_per_day):
                for name, organism in self.organisms.items():
                    time = day + (step / timesteps_per_day)
                    self.organisms[name].history.append([time, organism.population])
                    self.deltas[name] = 0

                    # Get pop change
                    if organism.is_producer:
                        self.deltas[organism.name] = organism.population * organism.reproduction_factor
                    else:
                        hunted = organism.food.hunting_factor * organism.population * self.organisms[organism.food.name].population
                        self.deltas[organism.name] = hunted - (organism.population * organism.death_rate)
                        self.deltas[organism.food.name] -= hunted * organism.food.biomass_ratio

                # Apply pop change
                for name, delta in self.deltas.items():
                    self.organisms[name].population += (delta / timesteps_per_day)
                    if self.organisms[name].population < 0:
                        self.organisms[name].population = 0



if __name__ == '__main__':
    env = Environment(10000)

    """
    Lotka-Volterra
    alpha = reproduction_factor
    beta = hunting_factor / biomass_ratio
    gamma = death_rate
    delta = hunting_factor
    """

    baboon = Producer("Baboons", reproduction_factor=0.11)
    cheetahs = Consumer("Cheetahs", Food("Baboons", hunting_factor=0.01, biomass_ratio=4), death_rate=0.04)

    env.add_organism(baboon, 10)
    env.add_organism(cheetahs, 10)

    env.simulate(1000)
    env.plot_all_histories()
