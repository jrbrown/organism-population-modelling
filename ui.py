# 05/09/2020
# Jason Brown
# Population modelling with sophisticated methods, UI file



from data.classes.environment import *
from data.classes.organism import *
from data.classes.event import *

from data.util import *



def remove_food_relationship_menu(organism):
    menu(
        lambda: f"{organism.food_sources_str()}\n\nRemove which food source?",
        [[food, organism.remove_food_relationship, food] for food in organism.food_relationships.keys()]
    )


def edit_food_relationship(food_object):
    menu(
        lambda: f"{food_object}{linebreak}{linebreak}Edit which value?",
        [
            ["Zero Growth Ratio", redefine_object_property, [food_object, "zero_growth_ratio"]],
            ["Max number eaten by individual eater", redefine_object_property, [food_object, "max_number_eaten_by_individual"]],
            ["Max chance individual food is eaten", redefine_object_property, [food_object, "max_chance_individual_is_eaten"]]
        ],
        repeat=True
    )


def edit_food_relationships_menu(organism):
    menu(
        lambda: f"{organism.food_sources_str()}\n\nWhich food source?",
        [[name, edit_food_relationship, food_object] for name, food_object in organism.food_relationships.items()],
        repeat=True
    )


def food_relationships_menu(organism, env):
    menu(
        lambda: f"{organism.food_sources_str()}\n\nSelect an option",
        [
            ["Add food relationship", organism.add_food_relationship_via_ui, env],
            ["Remove food relationship", remove_food_relationship_menu, organism],
            ["Edit food relationship", edit_food_relationships_menu, organism]
        ],
        repeat=True,
        question_header=1
    )


def view_edit_organism(organism, env):
    if yes_or_no(f"\n{organism}\n\nEdit organism?"):
        if type(organism) == Producer:
            menu_options = [
                ["Name", rename_object, organism],
                ["Population", redefine_object_property, [organism, "population"]],
                ["Max growth rate", redefine_object_property, [organism, "max_growth_rate"]]
            ]

        elif type(organism) == Consumer:
            menu_options = [
                ["Name", rename_object, organism],
                ["Population", redefine_object_property, [organism, "population"]],
                ["Max birth rate", redefine_object_property, [organism, "max_birth_rate"]],
                ["Starving death rate", redefine_object_property, [organism, "starving_death_rate"]],
                ["Food relationships", food_relationships_menu, [organism, env]]
            ]

        else:
            raise Exception("Unknown organism type oh-no")

        menu(
            lambda: f"\n{organism}\n\nWhat would you like to edit?",
            menu_options,
            repeat=True
        )


def organisms_menu(env):
    menu(
        lambda: f"{env}\n\nView/Edit which organism?",
        [[organism.name, view_edit_organism, [organism, env]] for organism in env.organisms.values()],
        repeat=True
    )


def review_environment(env):
    menu(
        lambda: f"{env}\n\nSelect an option",
        [
            ["View/Edit Organisms", organisms_menu, env],
            ["Edit environment size", redefine_object_property, [env, "max_producers"]],
            ["Rename environment", rename_object, env],
            ["Print environment info again", print_info, env]
        ],
        repeat=True
    )


def new_event():
    print("Unimplemented")
    enter_to_continue()


def environment_menu(env):
    menu(f"{env.name} loaded - Select an option:",
         menu_list=[
             ["Add Organism", Organism.create_organism_and_add_to_env, env],
             ["Remove Organism", env.remove_organism],
             ["Add Event", new_event],
             ["Review Environment", review_environment, env],
             ["Reset Environment", env.reset],
             ["Save Environment", env.save_environment],
             ["Simulate Environment", env.reset_sim_graph]
         ],
         question_header=1,
         repeat=True)


def get_env(method):
    if method == "new":
        environment_menu(Environment.new_environment())
    elif method == "load":
        environment_menu(Environment.load_environment())
    else:
        raise Exception("method arg of get_env must be \"new\" or \"load\"")


def main_menu():
    menu("""Organism Population Modelling
Main Menu""",
         menu_list=[
             ["Create new environment", get_env, "new"],
             ["Load environment", get_env, "load"],
         ],
         question_header=2,
         repeat=True)


if __name__ == '__main__':
    main_menu()
