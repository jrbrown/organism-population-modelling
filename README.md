# Organism Population Modelling

Allows the user to model predator / prey and prey / producer relationships on an arbitrary scale. First, using the python API you setup your animals and their relationships, then you run the simulation and a graph of the animal populations over time is shown. There's support in the API for adding events that can be set to trigger at certain times and influence the populations to simulate things like disease or culls.

## Using the Python API

### Do I need to know Python?

Not really, the API uses function calls to setup and execute the simulation. By looking at examples.py it should be clear how to do this. A simple setup can be achieved and simulated in just 8 short lines of code. Knowing Python would help you understand the underlying program as well as you giving you more power to expand and adjust the program to suit induvidual needs but it is not neccasary.

### How do I get started with the API?

Simply create a python file in the home directory and import the neccasary files. To see examples of the API in action look at examples.py. The API is very powerful and should allow modelling of most systems.

## Text UI

There is also a text based UI though this is unfinished and doesn't yet allow utilization of the event system. It's also a bit trickier to make quick small changes and does not support lower level execution (See examples.py). For this reason using the text UI is not recommended unless you are allergic to python.

### Limitations

This program does not (yet) model genetics, evolution, geography, environmental suitability or lots of other things. It simply allows the modelling of how animal populations change over time based on defined relationships. It can do this very well and supports arbitrary complexity of relationships. Using the events system and lower level execution methods even more complex things could be achieved.

### Technologies

This project was made with:

- Python 3.8
- Matplotlib 3.3.2
- Numpy 1.19.2
